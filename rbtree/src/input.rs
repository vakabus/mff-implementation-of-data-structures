use std::io::StdinLock;
use std::io::Lines;

#[derive(Debug)]
pub enum InputCommand {
    Insert(u32),
    Delete(u32),
    GetNth(u32),
}

pub struct InputCommandIterator<'a> {
    lines: Lines<StdinLock<'a>>,
}

impl InputCommandIterator<'_> {
    pub fn new<'a>(lines: Lines<StdinLock<'a>>) -> InputCommandIterator<'a> {
        InputCommandIterator {
            lines
        }
    }
}

impl Iterator for InputCommandIterator<'_> {
    type Item = InputCommand;
    fn next(&mut self) -> std::option::Option<InputCommand> {
        let n = self.lines.next();
        match n {
            None => None,
            Some(line) => {
                let line = line.unwrap();
                let mut arr = line.split_whitespace();
                let (cmd, num): (&str, u32) = (
                    arr.next().expect("chybí příkaz"),
                    arr.next()
                        .expect("chybí hodnota")
                        .parse()
                        .expect("hodnota není validní číslo"),
                );

                Some(match cmd {
                    "I" => InputCommand::Insert(num),
                    "F" => InputCommand::GetNth(num),
                    "D" => InputCommand::Delete(num),
                    _ => panic!("Neplatný vstup!"),
                })
            }
        }
    }
}
