mod input;
mod ordered_list;

use input::{InputCommand, InputCommandIterator};
use ordered_list::OrderedList;
use std::{
    collections::BTreeSet,
    io::{self, BufRead},
};

impl<T: Ord> OrderedList<T> for BTreeSet<T> {
    fn insert(&mut self, value: T) {
        let already_there = self.insert(value);
        if already_there {
            panic!("This dummy implementation of ordered list does not support duplicate values.");
        }
    }

    fn remove(&mut self, value: &T) -> bool {
        self.remove(value)
    }

    fn get_nth(&self, n: usize) -> Option<&T> {
        self.iter().skip((n - 1) as usize).take(1).next()
    }
}

fn main() {
    let mut tree = BTreeSet::<u32>::new();

    for cmd in InputCommandIterator::new(io::stdin().lock().lines()) {
        match cmd {
            InputCommand::GetNth(n) => {
                println!(
                    "{}",
                    tree.get_nth(n as usize)
                        .expect("invalid input, no such element in the tree")
                );
            }
            InputCommand::Delete(val) => {
                assert!(tree.remove(&val));
            }
            InputCommand::Insert(val) => {
                tree.insert(val);
            }
        }
    }
}
