pub trait OrderedList<T: Ord> {
    fn insert(&mut self, value: T);
    fn remove(&mut self, value: &T) -> bool;
    fn get_nth(&self, n: usize) -> Option<&T>;
}
